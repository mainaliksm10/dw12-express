import { Router } from "express";

export let firstRouter = Router();

firstRouter
  .route("/") // localhost:8000
  .post((req, res, next) => {
    // console.log("home post");
    res.json("home post");
  })
  .get((req, res, next) => {
    // console.log("home get");
    res.json("home get");
  })
  .patch((req, res, next) => {
    // console.log("home update");
    res.json("home update");
  })
  .delete((req, res, next) => {
    // console.log("home delete");
    res.json("home delete");
  });
