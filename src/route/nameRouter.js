import { Router } from "express";

export let nameRouter = Router();

nameRouter
  .route("/") 
  .post((req,res,next)=>{
    // console.log("name post")
    res.json("name post")
  })
  .get((req,res,next)=>{
    // console.log("name get")
    res.json("name get")
  })
  .patch((req,res,next)=>{
    // console.log("name update")
    res.json("name update")
  })
  .delete((req,res,next)=>{
    // console.log("name delete")
    res.json("name delete")
  })