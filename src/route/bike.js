import { Router } from "express";

export let bike = Router();

bike
  .route("/")
  .post(
    (req, res, next) => {
      console.log("I am middleware 1");
      let err = new Error("This is my first error");
      next(err);
    },
    (error, req, res, next) => {
      console.log("I am error middleware 1");
      console.log(error.message);
    },
    (req, res, next) => {
      console.log("I am middleware 2");
    },
    (error, req, res, next) => {
      console.log("I am error middleware 2");
    },
    (req, res, next) => {
      console.log("I am middleware 3");

      res.json({
        success: true,
        message: "Bike created successfully",
      });
    }
  )
  .get((req, res, next) => {
    // console.log("bike get")
    res.json("bike get");
  })
  .patch((req, res, next) => {
    // console.log("bike update")
    res.json("bike update");
  })
  .delete((req, res, next) => {
    // console.log("bike delete")
    res.json("bike delete");
  });

bike
  .route("/home") // static routing
  .get((req, res, next) => {
    console.log(req.body);
    res.json("bike name get");
  });

bike
  .route("/:id") // dynamic routing
  .get((req, res, next) => {
    // console.log(req.body)
    // res.json("id");
    console.log(req.params);
    res.json(req.query);
  });

bike.route("/:id1/name/:id").get((req, res, next) => {
  //   console.log(req.params);
  //   res.json(req.params);
  /* 
    req.query = {
        name="Kamalsheel"
        address="Chabahil"
    }
*/
});

// Always place dynamic route at end
