import { query } from "express";
import { Student } from "../schema/model.js";

export let createStudentService = async (data = {}) => {
  return await Student.create(data);
};

export let readAllStudentService = async (
  query = {},
  sort = null,
  skip = 0,
  limit = 10
) => {
  return await Student.find(query).sort(sort).skip(skip).limit(limit);
};

export let readSpecificStudentService = async (id = "") => {
  return await Student.findById(id);
};

export let updateStudentService = async (id = "", data = {}) => {
  return await Student.findByIdAndUpdate(id, data, {
    new: true,
  });
};
export let deleteStudentService = async (id = "") => {
  return await Student.findByIdAndDelete(id);
};
