import { Review } from "../schema/model.js";

export let createReviewService = async (data) => await Review.create(data);

export let readAllReviewService = async () => {
  return await Review.find({})
    .populate("user", "name")
    .populate("product", "price");
};

export let readSpecificReviewService = async (id) => {
  return await Review.findById(id);
};

export let updateReviewService = async (id, data) => {
  return await Review.findByIdAndUpdate(id, data, {
    new: true,
  });
};
export let deleteReviewService = async (id) => {
  return await Review.findByIdAndDelete(id);
};
