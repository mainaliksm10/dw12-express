import { Random } from "../schema/model.js";

export let createRandomService = async (data) => {
  return await Random.create(data);
};

export let readAllRandomService = async () => {
  return await Random.find({ name: "shambhu" });
};

export let readSpecificRandomService = async (id) => {
  return await Random.findById(id);
};
Random;

export let updateRandomService = async (id, data) => {
  return await Random.findByIdAndUpdate(id, data, {
    new: true,
  });
};
export let deleteRandomService = async (id) => {
  return await Random.findByIdAndDelete(id);
};
