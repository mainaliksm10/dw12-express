import { Student } from "../schema/model.js";
import {
  createStudentService,
  deleteStudentService,
  readAllStudentService,
  readSpecificStudentService,
  updateStudentService,
} from "../service/studentService.js";

export let createStudent = async (req, res, next) => {
  try {
    let result = await createStudentService(req.body);
    res.json({
      success: true,
      message: "Student created successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllStudent = async (req, res, next) => {
  let { totalDataInPage = 10, pageNo = 1, sort, ...query } = req.query;

  let skip = (Number(pageNo) - 1) * Number(totalDataInPage);
  let limit = Number(totalDataInPage);
  try {
    let result = await readAllStudentService(query, sort, skip, limit);
    res.status(200).json({
      success: true,
      message: "Student read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let readSpecificStudent = async (req, res, next) => {
  try {
    let result = await readSpecificStudentService(req.params.id);
    res.json({
      success: true,
      message: "Student read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let updateStudent = async (req, res, next) => {
  try {
    let result = updateStudentService(req.params.id, req.body);
    res.json({
      success: true,
      message: "Students update successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteStudent = async (req, res, next) => {
  try {
    let result = await deleteStudentService(req.params.id);
    res.json({
      success: true,
      message: "Students deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
