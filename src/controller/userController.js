import { User } from "../schema/model.js";
import {
  createUserService,
  deleteUserService,
  readAllUserService,
  readSpecificUserService,
  updateUserService,
} from "../service/userService.js";
import bcrypt from "bcrypt";
import { sendEmail } from "../utils/sendmail.js";

export let createUserController = async (req, res, next) => {
  let data = req.body;
  data.password = await bcrypt.hash(data.password, 10);

  try {
    let result = await createUserService(data);

    // for (let i = 1; i <= 20; i++) {
    //   await sendEmail({
    //     to: ["user"],
    //     subject: "Alert!Alert!Alert!",
    //     html: `
    //     <h1> Dear User</h1>,
    //     <p>You have successfully registered in our system.</p>
    //     `,
    //   });
    // }
    await sendEmail({
      // to: ["mainali.sheel@gmail.com"],
      to: [req.body.email],
      subject: "Alert!Alert!Alert!",
      html: `
      <h1> Dear User,</h1>
      <p>Please verify your email by clicking the link we've sent. Welcome aboard!</p>
      `,
    });

    res.status(201).json({
      success: true,
      message: "User created successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllUserController = async (req, res, next) => {
  try {
    let result = await readAllUserService();
    res.json({
      success: true,
      message: "User read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let readSpecificUserController = async (req, res, next) => {
  try {
    let result = await readSpecificUserService(req.params.id);
    res.json({
      success: true,
      message: "User read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let updateUserController = async (req, res, next) => {
  try {
    let result = updateUserService(req.params.id, req.body);
    res.json({
      success: true,
      message: "Users update successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteUserController = async (req, res, next) => {
  try {
    let result = await deleteUserService(req.params.id);
    console.log("****", result);
    res.json({
      success: true,
      message: "Users deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let loginUserController = async (req, res) => {
  let email = req.body.email;
  let password = req.body.password;

  let user = await User.findOne({ email: email });

  if (user === null) {
    res
      .status(404)
      .json({ success: false, message: "email or password does not match" });
  } else {
    let dbPassword = user.password;
    let isValidPassword = await bcrypt.compare(password, dbPassword);

    if (isValidPassword) {
      res.status(200).json({ success: true, message: "login successfully" });
    } else {
      res
        .status(400)
        .json({ success: false, message: "email or password does not match" });
    }
  }
};
