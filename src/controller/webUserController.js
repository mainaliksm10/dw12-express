import bcrypt, { hash } from "bcrypt";
import jwt from "jsonwebtoken";
import { secretKey } from "../constant.js";
import { WebUser } from "../schema/model.js";
import { sendEmail } from "../utils/sendmail.js";

export let createWebUserController = async (req, res, next) => {
  try {
    let data = req.body;
    let hashPassword = await bcrypt.hash(data.password, 10);
    data = {
      ...data,
      isVerifiedEmail: false,
      password: hashPassword,
    };
    let result = await WebUser.create(data);

    let infoObj = {
      _id: result._id,
    };

    let expiryInfo = {
      expiresIn: "5d",
    };
    let token = await jwt.sign(infoObj, secretKey, expiryInfo);

    await sendEmail({
      // from: '"Unique" <uniquekc425@gmail.com>',
      to: data.email,
      subject: "Account Verification",
      html: `
      <h1>Your account has successfully been created</h1>
      <a href="clientUrl/reset-password?token=${token}">
      clientUrl/verify-email?token=${token}
      </a>

      
      `,
    });

    res.status(201).json({
      success: true,
      message: "User created successfully",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const verifyEmail = async (req, res, next) => {
  try {
    let tokenString = req.headers.authorization;
    let tokenArray = tokenString.split(" ");
    let token = tokenArray[1];

    //verify token
    let infoObj = await jwt.verify(token, secretKey);
    let userId = infoObj._id;

    let result = await WebUser.findByIdAndUpdate(
      userId,
      {
        isVerifiedEmail: true,
      },
      {
        new: true,
      }
    );

    res.status(200).json({
      success: true,
      message: "WebUser verified successfully",
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const loginUser = async (req, res, next) => {
  try {
    let email = req.body.email;
    let password = req.body.password;

    let user = await WebUser.findOne({ email: email });

    if (user) {
      if (user.isVerifiedEmail) {
        let isValidPassword = await bcrypt.compare(password, user.password);
        if (isValidPassword) {
          let infoObj = {
            _id: user._id,
          };
          let expiryInfo = {
            expiresIn: "365d",
          };

          let token = await jwt.sign(infoObj, secretKey, expiryInfo);

          res.status(200).json({
            success: true,
            message: "user login successful",
            data: user,
            token: token,
          });
        } else {
          let error = new Error("credential doesn't match");
          throw error;
        }
      } else {
        let error = new Error("credential doesn't match");
        throw error;
      }
    } else {
      let error = new Error(
        "Credential not found, enter the valid Information"
      );
      throw error;
    }
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const myProfile = async (req, res, next) => {
  try {
    let _id = req._id;

    let result = await WebUser.findById(_id);
    res.status(200).json({
      success: true,
      message: "profile read successfully",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: "unable to read my profile",
    });
  }
};

export const updateProfile = async (req, res, next) => {
  try {
    let _id = req._id;
    let data = req.body;
    delete data.email;
    delete data.password;
    let result = await WebUser.findByIdAndUpdate(_id, data, {
      new: true,
    });
    res.status(201).json({
      success: true,
      message: "Profile updated successfully.",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      error: error.message,
    });
  }
};

export const updatePassword = async (req, res, next) => {
  try {
    let _id = req._id;
    let oldPassword = req.body.oldPassword;
    let newPassword = req.body.newPassword;

    let data = await WebUser.findById(_id);
    let hashPassword = data.password;

    let isValid = await bcrypt.compare(oldPassword, hashPassword);
    if (isValid) {
      let newHashPassword = await bcrypt.hash(newPassword, 10);
      let result = await WebUser.findByIdAndUpdate(
        _id,
        {
          password: newHashPassword,
        },
        { new: true }
      );
      res.status(201).json({
        success: true,
        message: "Password updated successfully.",
        data: result,
      });
    } else {
      let error = new Error("Credential does not match.");
      throw error;
    }
    console.log(data);
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const readAllUser = async (req, res, next) => {
  try {
    let result = await WebUser.find({});
    res.status(200).json({
      success: true,
      message: "All user read successfully",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const readSpecificUser = async (req, res, next) => {
  try {
    let id = req.params.id;

    let result = await WebUser.findById(id);
    res.status(200).json({
      success: true,
      message: "All user read successfully",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const updateSpecificUser = async (req, res, next) => {
  try {
    let id = req.params.id;
    let data = req.body;
    delete data.email;
    delete data.password;

    let result = await WebUser.findByIdAndUpdate(id, data, { new: true });

    res.status(201).json({
      success: true,
      message: "User updated successfully.",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const deleteSpecificUser = async (req, res, next) => {
  try {
    let id = req.params.id;

    let result = await WebUser.findByIdAndDelete(id);
    res.status(200).json({
      success: true,
      message: "User deleted successfully.",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const forgotPassword = async (req, res, next) => {
  try {
    let email = req.body.email;

    let result = await WebUser.findOne({ email: email });

    if (result) {
      let infoObj = {
        _id: result._id,
      };
      let expiryInfo = {
        expiresIn: "5d",
      };

      let token = jwt.sign(infoObj, secretKey, expiryInfo);

      await sendEmail({
        // from: '"Unique" <uniquekc425@gmail.com>',
        to: email,
        subject: "Password Reset",
        html: `
        <h1>Please click the given link to reset your password.</h1>
        <a href="clientUrl/reset-password?token=${token}">
        clientUrl/reset-password?token=${token}
        </a>
  
        
        `,
      });

      res.status(200).json({
        success: true,
        message: "To reset password, please check your email!",
      });
    } else {
      res.status(404).json({
        success: false,
        message: "email is not found",
      });
    }
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const resetPassword = async (req, res, next) => {
  try {
    let hashPassword = await bcrypt.hash(req.body.password, 10);

    let result = await WebUser.findByIdAndUpdate(
      req._id,
      {
        password: hashPassword,
      },
      { new: true }
    );

    res.status(201).json({
      success: true,
      message: "Password reset successfully.",
      conclusion: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
