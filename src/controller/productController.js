import { Product } from "../schema/model.js";
import productSchema from "../schema/productSchema.js";
import {
  createProductService,
  deleteProductService,
  readAllProductService,
  readSpecificProductService,
  updateProductService,
} from "../service/productService.js";

export let createProductController = async (req, res, next) => {
  try {
    let result = await createProductService(req.body);
    res.json({
      success: true,
      message: "Product created successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllProductController = async (req, res, next) => {
  try {
    let result = await readAllProductService();
    res.json({
      success: true,
      message: "Product read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let readSpecificProductController = async (req, res, next) => {
  try {
    let result = await readSpecificProductService(req.params.id);
    res.json({
      success: true,
      message: "Product read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let updateProductController = async (req, res, next) => {
  try {
    let result = updateProductService(req.params.id, req.body);
    res.json({
      success: true,
      message: "Products update successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteProductController = async (req, res, next) => {
  try {
    let result = await deleteProductService(req.params.id);
    res.json({
      success: true,
      message: "Products deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
