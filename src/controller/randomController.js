import { Random } from "../schema/model.js";
import {
  createRandomService,
  deleteRandomService,
  readAllRandomService,
  readSpecificRandomService,
  updateRandomService,
} from "../service/randomService.js";

export let createRandomController = async (req, res, next) => {
  // console.log("******");
  // console.log(req.body);
  try {
    let result = await createRandomService(req.body);
    res.json({
      success: true,
      message: "Random created successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllRandomController = async (req, res, next) => {
  try {
    let result = await readAllRandomService();
    res.json({
      success: true,
      message: "Random read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let readSpecificRandomController = async (req, res, next) => {
  try {
    let result = await readSpecificRandomService(req.params.id);
    res.json({
      success: true,
      message: "Random read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let updateRandomController = async (req, res, next) => {
  try {
    let result = updateRandomService(req.params.id, req.body);
    res.json({
      success: true,
      message: "Randoms update successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteRandomController = async (req, res, next) => {
  try {
    let result = await deleteRandomService(req.params.id);
    console.log("****", result);
    res.json({
      success: true,
      message: "Randoms deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
