export let uploadSingleFile = (req, res, next) => {
  let link = `serverUrl/${req.file.filename}`;
  res.json({
    success: true,
    message: "file uploaded successfully",
    result: link,
  });
};
export let uploadMultipleFile = (req, res, next) => {
  console.log(req.files);
  let path = req.files;
  let link = path.map((val, i) => {
    if (val.filename) {
      return `serverUrl/${val.filename}`;
    }
  });
  res.json({
    success: true,
    message: "file uploaded successfully",
    result: link,
  });
};
