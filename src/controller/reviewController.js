import { Review } from "../schema/model.js";
import {
  createReviewService,
  deleteReviewService,
  readAllReviewService,
  readSpecificReviewService,
  updateReviewService,
} from "../service/reviewService.js";

export let createReviewController = async (req, res, next) => {
  // console.log("******");
  // console.log(req.body);
  try {
    let result = await createReviewService(req.body);
    res.json({
      success: true,
      message: "Review created successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllReviewController = async (req, res, next) => {
  try {
    let result = await readAllReviewService();
    res.json({
      success: true,
      message: "Review read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let readSpecificReviewController = async (req, res, next) => {
  try {
    let result = await readSpecificReviewService(req.params.id);
    res.json({
      success: true,
      message: "Review read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let updateReviewController = async (req, res, next) => {
  try {
    let result = updateReviewService(req.params.id, req.body);
    res.json({
      success: true,
      message: "Reviews update successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteReviewController = async (req, res, next) => {
  try {
    let result = await deleteReviewService(req.params.id);
    console.log("****", result);
    res.json({
      success: true,
      message: "Reviews deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
