import { Schema } from "mongoose";

let studentSchema = Schema({
  name: {
    required: true,
    type: String,
    // lowercase:true,
    // uppercase: true,
    required: [true, "name field is required"], // error field must resemble with field name
    minLength: [3, "name field must be more than 3 characters"],
    maxLength: [25, "name field must be less than 25 characters"],
  },
  age: {
    required: true,
    type: Number,
  },
  roll: {
    type: Number,
    min: [400, "roll must be more than 400"],
    max: [500, "roll must be less than 500"],
  },
  phoneNumber: {
    type: Number,
    // min: [1000000000, "The length of phoneNumber must be less than 10"],
    // max: [9999999999, "The length of phoneNumber must be more than 10"],
    validate: (value) => {
      if (value.toString().length !== 10) {
        throw new Error("Phone number must be exact 10 digits");
      }
    },
  },
  isMarried: {
    required: true,
    type: Boolean,
    default: false,
  },
  gender: {
    type: String,
    required: true,
    default: "male",
    validate: (value) => {
      if (value === "male" || value === "female" || value === "other") {
      } else {
        throw new Error("Invalid Gender");
      }
    },
  },
  dob: {
    type: Date,
    default: Date.now,
  },
});

export default studentSchema;
