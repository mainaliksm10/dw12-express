import { Schema } from "mongoose";

let productSchema = Schema({
  name: {
    required: true,
    type: String,
  },
  quantity: {
    required: true,
    type: Number,
  },
  price: {
    required: true,
    type: Number,
  },
});

export default productSchema;
