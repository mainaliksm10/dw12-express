import { Schema } from "mongoose";

let classRoomSchema = Schema({
  name: {
    required: true,
    type: String,
  },
  numberofBench: {
    required: true,
    type: Number,
  },
  hasTv: {
    required: true,
    type: Number,
  },
});

export default classRoomSchema;
