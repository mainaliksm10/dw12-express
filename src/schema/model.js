import { model } from "mongoose";
import bookSchema from "./bookSchema.js";
import classRoomSchema from "./classRoom.js";
import collegeSchema from "./college.js";
import departmentSchema from "./department.js";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import traineeSchema from "./trainee.js";
import productSchema from "./productSchema.js";
import reviewSchema from "./ reviewSchema.js";
import userSchema from "./userSchema.js";
import randomSchema from "./randomSchema.js";
import webUserSchema from "./webUserSchema.js";

export let Student = model("Student", studentSchema);
export let Teacher = model("Teacher", teacherSchema);
export let Book = model("Book", bookSchema);
export let Trainee = model("Trainee", traineeSchema);
export let College = model("College", collegeSchema);
export let ClassRoom = model("ClassRoom", classRoomSchema);
export let Department = model("Department", departmentSchema);
export let Product = model("Product", productSchema);
export let User = model("User", userSchema);
export let Review = model("Review", reviewSchema);
export let Random = model("Random", randomSchema);
export let WebUser = model("WebUser", webUserSchema);
/* 
    first letter of  model name:

*/
