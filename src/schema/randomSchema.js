import { Schema } from "mongoose";

let randomSchema = Schema({
  name: {
    type: String,
  },
  age: {
    type: Number,
    // required: "Age field is required",
  },
  password: {
    type: String,
    // required:"Password is required"
  },
  phoneNumber: {
    type: Number,
  },
  roll: {
    type: Number,
  },
  spouseName: {
    type: String,
  },
  email: {
    type: String,
  },
  gender: {
    type: String,
  },
  dob: {
    type: Date,
  },
  location: {
    country: {
      type: String,
    },
    exactLocation: {
      type: String,
    },
  },
});

export default randomSchema;
