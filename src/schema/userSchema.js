import { Schema } from "mongoose";

const userSchema = Schema({
  fullName: {
    type: String,
    required: true,
    trim: true,
  },
  address: {
    type: String,
    trim: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    trim: true,
  },
  password: {
    type: String,
    required: true,
    trim: true,
  },
  phoneNumber: {
    type: String, // Change the type to String
    required: true,
    trim: true,
  },
  gender: {
    type: String,
    required: true,
    trim: true,
    default: "male",
  },
  dob: {
    type: Date,
    required: true,
  },
  isMarried: {
    type: Boolean,
    trim: true,
  },
});

export default userSchema;
