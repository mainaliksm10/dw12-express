import { Router } from "express";
import {
  createRandomController,
  deleteRandomController,
  readAllRandomController,
  readSpecificRandomController,
  updateRandomController,
} from "../controller/randomController.js";
import Joi from "joi";
import validation from "../middleware/validation.js";

let randomValidation = Joi.object()
  .keys({
    name: Joi.string().required().min(3).max(10),
    age: Joi.number()
      .min(10)
      .custom((value, msg) => {
        if (value <= 20) {
          return true;
        } else {
          return msg.message(
            "To get free pass, your age must be less than 19!"
          );
        }
      }),
    password: Joi.string()
      .required()
      .custom((value, msg) => {
        let isValidPassword = value.match(
          /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/
        );
        if (isValidPassword) {
          return true;
        } else {
          return msg.message(
            "Your password must:\n- Be at least 8 characters long\n- Contain at least one uppercase letter\n- Contain at least one lowercase letter\n- Contain at least one digit\n- Contain at least one special character (!, @, #, $, etc.)"
          );
        }
      }),
    phoneNumber: Joi.number(),
    roll: Joi.number(),
    dob: Joi.date(),
    email: Joi.string()
      .required()
      .custom((value, msg) => {
        let validEmail = value.match(
          /[A-Za-z0-9._%+-]+@(gmail\.com|yahoo\.com|hotmail\.com)/
        );
        if (validEmail) {
          return true;
        } else {
          return msg.message("Invalid Email");
        }
      }).message({
        "any.required": "name is required",
      }),
    gender: Joi.string().required().valid("male", "female", "other"),
    location: Joi.object().keys({
      location: Joi.string().required(),
      exactLocation: Joi.string().required(),
    }),
  })
  .unknown(false);

let randomRouter = Router();

randomRouter
  .route("/")
  .post(validation(randomValidation), createRandomController)
  .get(readAllRandomController);

randomRouter
  .route("/:id")
  .get(readSpecificRandomController)
  .patch(updateRandomController)
  .delete(deleteRandomController);
export default randomRouter;
