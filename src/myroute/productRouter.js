import { Router } from "express";
import {
  createProductController,
  deleteProductController,
  readAllProductController,
  readSpecificProductController,
  updateProductController,
} from "../controller/productController.js";

// let canAddProduct = (role) => {
//   return (req, res, next) => {
//     if (role === "admin") {
//       next();
//     } else if (role === "customer") {
//       res.status(404).json({
//         success: false,
//         message: "You are not allowed to create public product",
//       });
//     }
//   };
// };

// let ageGreaterThan18 = (age) => {
//   return (req, res, next) => {
//     if (age > 18) {
//       next();
//     } else if (age < 18) {
//       res.status(404).json({
//         success: false,
//         message:
//           "You are not allowed to create public product as your age is less than 18.",
//       });
//     }
//   };
// };

// let isMarried = (age, status) => {
//   return (req, res, next) => {
//     if (age > 18 && status === "Married") {
//       next();
//     } else if (age < 18 && status !== "Married") {
//       res.status(404).json({
//         success: false,
//         message:
//           "Either your age is less than 18 or you are not married, or both.",
//       });
//     }
//   };
// };

// let isAuthorized = (value) => {
//   return (req, res, next) => {
//     if (value === "admin" || value === "superadmin") {
//       next();
//     } else {
//       console.log("You cannot delete the user");
//     }
//   };
// };

// let deleteUser = (req, res, next) => {
//   console.log("User deleted successfully");
// };

let productRouter = Router();

productRouter
  .route("/")
  .post(createProductController)
  // .post(canAddProduct("customer"), createProductController)
  // .post(isAuthorized("customer"), deleteUser)
  // .post(ageGreaterThan18("20"), createProductController)
  // .post(isMarried("20", "married"), createProductController)
  .get(readAllProductController);

productRouter
  .route("/:id")
  .get(readSpecificProductController)
  .patch(updateProductController)
  .delete(deleteProductController);
export default productRouter;
