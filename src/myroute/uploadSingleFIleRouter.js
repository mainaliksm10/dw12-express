import { Router } from "express";
import {
  uploadMultipleFile,
  uploadSingleFile,
} from "../controller/uploadSIngleFileController.js";
import upload from "../utils/uploadFile.js";

let uploadSingleFileRouter = Router();

uploadSingleFileRouter
  .route("/single")
  .post(upload.single("file1"), uploadSingleFile);

uploadSingleFileRouter.route("/multiple").post(upload.array("file1"),uploadMultipleFile);

export default uploadSingleFileRouter;
