import mongoose from "mongoose";
import { dbUrl } from "../constant.js";

const connectToMongoDb = async () => {
  try {
    await mongoose.connect(dbUrl);
    console.log(`application is connected to mongod successfully at s${dbUrl}`);
  } catch (error) {
    // let error1 = new Error("Unable to connect");
    // throw error1;
  }
};

export default connectToMongoDb;
