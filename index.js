import { config } from "dotenv";
import express, { json } from "express";
import connectToMongoDb from "./src/connectdb/connectToMongoDb.js";
import productRouter from "./src/myroute/productRouter.js";
import randomRouter from "./src/myroute/randomRouter.js";
import reviewRouter from "./src/myroute/reviewRouter.js";
import studentRouter from "./src/myroute/studentRouter.js";
import uploadSingleFileRouter from "./src/myroute/uploadSingleFIleRouter.js";
import userRouter from "./src/myroute/userRouter.js";
import webUserRouter from "./src/myroute/webUserRouter.js";
import { bike } from "./src/route/bike.js";
import { firstRouter } from "./src/route/firstRouter.js";
import { nameRouter } from "./src/route/nameRouter.js";
import { traineesRouter } from "./src/route/traineesRouter.js";
import { port } from "./src/constant.js";
let expressApp = express();
connectToMongoDb();

config();

// expressApp.use(
//   (req, res, next) => {
//     console.log("I am application, normal middleware");
//     let err = new Error("This is my very first error in index");
//     next(err);
//   },
//   (error, req, res, next) => {
//     console.log("I am application, error middleware");
//     next();
//   },
//   (req, res, next) => {
//     console.log("I am application, normal middleware");
//     next();
//   }
// );

expressApp.use(express.static("./public")); // localhost:8000/profileImage.webp
expressApp.use(json()); // It is done to make our application to accept json data
expressApp.use("/students", studentRouter);
expressApp.use("/trainees", traineesRouter);
expressApp.use("/", firstRouter);
expressApp.use("/names", nameRouter);
expressApp.use("/bikes", bike);
expressApp.use("/products", productRouter);
expressApp.use("/users", userRouter);
expressApp.use("/reviews", reviewRouter);
expressApp.use("/randoms", randomRouter);
expressApp.use("/web-users", webUserRouter);
expressApp.use("/upload-single-file-router", uploadSingleFileRouter);

expressApp.listen(port, () => {
  console.log("app is listening at port 8000");
});
