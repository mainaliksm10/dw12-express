import bcrypt from "bcrypt";

/* 
    Hashing
        Encryption: It is possible
        Decryption: It is not possible
*/

// ******** Generate hash code
// let hashCode = await bcrypt.hash("Kamalsheel", 10);
// console.log(hashCode);

// Compare hash code
let isValidPassword = await bcrypt.compare(
  "Kamalsheel", // password
  "$2b$10$9EG8z3qQbTuz1PWBdEVuVujGWcORcb7lJnpU5OrciH1qP.XZevyXe" //hashPassword
);
// the output will be true if hashPassword is made from the password


console.log(isValidPassword);
