import jwt from "jsonwebtoken";

let infoObj = {
  id: "1234",
};
let secretKey = "dw12";
let expiryInfo = {
  expiresIn: "365d",
}; // expiryInfo must be in same format

// let token=jwt.sign(infoObj,secretKey,expiryInfo)
// console.log(token)

let tokenCheck =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMzQiLCJpYXQiOjE3MDk3MTc4NjAsImV4cCI6MTc0MTI1Mzg2MH0.coQF4jA0X2hW6l-rbcogk_NFcAvR-n6qg4nW7Xijm8c";

try {
  let tokenVerify = jwt.verify(tokenCheck, "dw12");
  console.log(tokenVerify);
} catch (error) {
  console.log(error.message);
}
